package com.kii.example.saveuserprot;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kii.cloud.storage.KiiUser;
import com.kii.cloud.storage.callback.KiiUserCallBack;

/**
 * Created by satoshi on 2014/04/22.
 */
public class LoginFragment extends DialogFragment {

    private EditText usernameEdit;
    private EditText passwordEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View myView = inflater.inflate(R.layout.login_dialog, container);
        usernameEdit = (EditText) myView.findViewById(R.id.editText_username);
        passwordEdit = (EditText) myView.findViewById(R.id.editText_password);
        ((Button) myView.findViewById(R.id.button_login)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialogFragment pdf = new ProgressDialogFragment();
                LoginFragment.this.getChildFragmentManager().beginTransaction().add(pdf, "Progress").commit();

                String username = usernameEdit.getText().toString();
                String password = passwordEdit.getText().toString();
                KiiUser user = KiiUser.builderWithName(username).build();
                KiiUser.logIn(new KiiUserCallBack() {
                    @Override
                    public void onLoginCompleted(int token, KiiUser user, Exception exception) {
                        ((LoginFragmentCallback) LoginFragment.this.getActivity()).onLoginCompleted(exception);
                        pdf.dismiss();
                        LoginFragment.this.dismiss();
                        if (exception != null) {
                            exception.printStackTrace();
                            Toast.makeText(LoginFragment.this.getActivity(), "Failed to login.", Toast.LENGTH_LONG).show();
                            return;
                        }
                        Toast.makeText(LoginFragment.this.getActivity(), "Login Succeeded.", Toast.LENGTH_LONG).show();
                    }
                }, username, password);
            }
        });
        ((Button) myView.findViewById(R.id.button_register)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialogFragment pdf = new ProgressDialogFragment();
                LoginFragment.this.getChildFragmentManager().beginTransaction().add(pdf, "Progress").commit();

                String username = usernameEdit.getText().toString();
                String password = passwordEdit.getText().toString();
                KiiUser user = KiiUser.builderWithName(username).build();
                user.register(new KiiUserCallBack() {
                    @Override
                    public void onRegisterCompleted(int token, KiiUser user, Exception exception) {
                        pdf.dismiss();
                        ((LoginFragmentCallback)LoginFragment.this.getActivity()).onRegisterCompleted(exception);
                        LoginFragment.this.dismiss();
                        if (exception != null) {
                            exception.printStackTrace();
                            Toast.makeText(LoginFragment.this.getActivity(), "Failed to register.", Toast.LENGTH_LONG).show();
                            return;
                        }
                        Toast.makeText(LoginFragment.this.getActivity(), "Register Succeeded.", Toast.LENGTH_LONG).show();

                    }
                }, password);
            }
        });
        this.getDialog().setTitle("Sign Up to Kii Cloud!");
        return myView;
    }

    public interface LoginFragmentCallback {

        public void onLoginCompleted(Exception e);

        public void onRegisterCompleted(Exception e);
    }

}
