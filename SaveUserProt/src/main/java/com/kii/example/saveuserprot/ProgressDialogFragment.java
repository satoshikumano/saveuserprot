package com.kii.example.saveuserprot;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by satoshi on 2014/04/23.
 */
public class ProgressDialogFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog pd = new ProgressDialog(this.getActivity());
        pd.setIndeterminate(true);
        return pd;
    }
}
