package com.kii.example.saveuserprot;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.kii.cloud.storage.Kii;
import com.kii.cloud.storage.KiiUser;
import com.kii.cloud.storage.KiiUserStore;
import com.kii.cloud.storage.callback.KiiUserCallBack;
import com.kii.cloud.storage.exception.UserLoadException;

import java.io.IOException;


public class MainActivity extends ActionBarActivity implements LoginFragment.LoginFragmentCallback {

    private TextView textMain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Kii.initialize("f53533b9","f61589d47a693b2ad326392250b9d070", Kii.Site.JP);

        setContentView(R.layout.activity_main);
        textMain = (TextView) findViewById(R.id.text_main);

        findViewById(R.id.button_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textMain.setText("Text Cleared");
            }
        });

        findViewById(R.id.button_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KiiUser.logOut();
                LoginFragment lFragment = new LoginFragment();
                MainActivity.this.getSupportFragmentManager().beginTransaction().add(lFragment, "Login").commit();
            }
        });

        findViewById(R.id.button_refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (KiiUser.isLoggedIn()) {
                    textMain.setText(KiiUser.getCurrentUser().getUsername());
                } else {
                    textMain.setText("No user logged in");
                }
            }
        });

        if (!KiiUser.isLoggedIn()) {
            String token = null;
            if (savedInstanceState != null)
                token = savedInstanceState.getString("token");
            if (token != null) {
                final ProgressDialogFragment pdf = new ProgressDialogFragment();
                this.getSupportFragmentManager().beginTransaction().add(pdf, "Progress").commit();
                KiiUser.loginWithToken(new KiiUserCallBack() {
                    @Override
                    public void onLoginCompleted(int token, KiiUser user, Exception exception) {
                        pdf.dismiss();
                        if (exception != null) {
                            exception.printStackTrace();
                            LoginFragment lFragment = new LoginFragment();
                            MainActivity.this.getSupportFragmentManager().beginTransaction().add(lFragment, "Login").commit();
                            return;
                        }
                        textMain.setText(KiiUser.getCurrentUser().getUsername());
                    }
                }, token);
            } else {
                LoginFragment lFragment = new LoginFragment();
                this.getSupportFragmentManager().beginTransaction().add(lFragment, "Login").commit();
            }
        } else {
            textMain.setText(KiiUser.getCurrentUser().getUsername());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String token = KiiUser.getCurrentUser().getAccessToken();
        outState.putString("token", token);
    }

    @Override
    public void onLoginCompleted(Exception e) {
        if (e != null)
            return;
        textMain.setText(KiiUser.getCurrentUser().getUsername());
    }

    @Override
    public void onRegisterCompleted(Exception e) {
        if (e != null)
            return;
        textMain.setText(KiiUser.getCurrentUser().getUsername());
    }

}
